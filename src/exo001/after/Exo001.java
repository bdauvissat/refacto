package exo001.after;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

public class Exo001 {

    private static final double PCT_MAX = 100;

    public void calculPartGateau(List<String> gourmands) {

        if (gourmands == null || gourmands.size() == 0) {
            return;
        }

        double taille = gourmands.size();
        System.out.println("Nombre de parts : " + taille);

        afficheGourmands(gourmands);

        System.out.println("Pourcentage pour chaque mangeur : " + calculPourcentage(taille));

    }

    private String calculPourcentage(double taille) {

        double pct = PCT_MAX / taille;

        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        decimalFormat.setRoundingMode(RoundingMode.HALF_UP);
        return decimalFormat.format(pct);
    }

    private void afficheGourmands(List<String> gourmands) {

        for (String gourmand : gourmands) {
            System.out.println(gourmand);
        }

    }

}
