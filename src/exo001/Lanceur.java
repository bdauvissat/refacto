package exo001;

import exo001.after.Exo001;
import exo001.Before.exo001;

import java.util.List;

public class Lanceur {

    public static void main(String[] args) {
        exo001 avant = new exo001();
        Exo001 apres = new Exo001();

        System.out.println("Utilisation du code non modifi�");

        List<String> mangeurs = List.of("Syd", "Nick", "Roger", "David", "Richard");
        avant.Calculpartgateau(mangeurs);

        System.out.println("Utilisation du code apr�s modification");
        apres.calculPartGateau(mangeurs);

    }

}
